const sumar = require("../index");
const assert = require("assert");

// assertion
describe("test the sum of two numbers", ()=>{
  // affirm that 5+5=10
  it('five plus five is ten.', ()=>{
    assert.equal(10, sumar(5,5));
  });
  // affirm that five plus five is not fittyfive
  it("affirm that five plus five is not fittyfive", ()=>{
    assert.notEqual("55", sumar(5,5));
  });
});
